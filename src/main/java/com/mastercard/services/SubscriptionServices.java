package com.mastercard.services;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mastercard.dao.UserRepository;
import com.mastercard.dao.Users;

@RestController
public class SubscriptionServices {
	@Autowired
	UserRepository UserRepository;
	
	private static final Logger logger = Logger.getLogger(SubscriptionServices.class);
	
	@RequestMapping(name="/hello",method=RequestMethod.GET)
	public String sayHello(){
		logger.info("****************Hello");
		Iterable<Users> users=UserRepository.findAll();
		for(Users user:users){
			System.out.println(user.getUserName());
		}
		return "Hello";
		
	}
	
}

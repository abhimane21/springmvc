package com.mastercard.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ConfigProperties {
    @Value("${db.url}")
    private String dbUrl;
    @Value("${db.username}")
    private String userName;
    @Value("${db.password}")
    private String pasword;
    @Value("${db.driver-class}")
    private String datasourceDriver;
  
    @Value("${hibernate.dialect}")
    private String hibernateDialect;
    @Value("${hibernate.show_sql}")
    private String showSQL;
    @Value("${hibernate.format_sql}")
    private String formarSQL;
    
	public String getDbUrl() {
		return dbUrl;
	}
	public void setDbUrl(String dbUrl) {
		this.dbUrl = dbUrl;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPasword() {
		return pasword;
	}
	public void setPasword(String pasword) {
		this.pasword = pasword;
	}
	public String getDatasourceDriver() {
		return datasourceDriver;
	}
	public void setDatasourceDriver(String datasourceDriver) {
		this.datasourceDriver = datasourceDriver;
	}
	public String getHibernateDialect() {
		return hibernateDialect;
	}
	public void setHibernateDialect(String hibernateDialect) {
		this.hibernateDialect = hibernateDialect;
	}
	public String getShowSQL() {
		return showSQL;
	}
	public void setShowSQL(String showSQL) {
		this.showSQL = showSQL;
	}
	public String getFormarSQL() {
		return formarSQL;
	}
	public void setFormarSQL(String formarSQL) {
		this.formarSQL = formarSQL;
	}
}

